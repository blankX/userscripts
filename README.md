# User Scripts

User scripts I made to deal with some itches

## Abort Network Requests on Escape

A userscript that automatically cancels network requests initiated by
Javascript when Escape is pressed

## Anti-Thot

Hacky script to get rid of thot notifications

## Archive Site Links

A userscript that adds buttons to the monkey menu that lets you open sites in
the Wayback Machine and archive.today; also lets you archive pages

## Auto Cloudflare Email Protection Decoder

A reimplementation of Cloudflare's email protection code so that I don't have
to enable scripts to see emails again, inspired by
[this article](https://web.archive.org/web/20210120041753/https://resynth1943.net/articles/cloudflares-email-protection-does-not-work/)

## Catbox Max Files Disabler

Hacky script to disable the client-side file limit per tab

## Compass QoL Enhancer

A userscript that adds small but useful features for Compass. Features are:
- The ability to close windows by clicking on the background
- Calendar events now being links (they work with [Link Hints] now! you can
  also ctrl+click on "standard classes", events, and learning tasks)
- Calendar events now show their end time without having to being hovered
- Tabs are now links (you can refresh pages and the tab will automatically
  open. you can also ctrl+click on them)
- Files and folders in Resources are now marked clickable ([Link Hints] can now
  open them!)
- Revealing extra features in Resources (such as filter, created by, and
  permission information)
- File upload buttons now work with [Link Hints]
- Fix submitting links by simply pressing Enter
- Fix opening links with ampersands submitted in Learning Tasks
- Links inside lesson plans now open in the parent tab by default instead of
  creating a new tab
- Files and links submitted to learning tasks are now actual links (they now
  work with [Link Hints] and they can be ctrl-clicked)
- Ctrl-clicking an activity in a user's learning tasks tab no longer collapses
  everything else
- Learning tasks now being links (you can ctrl-click them)
- Opening a learning task will automatically add its ID to the URL
- Learning tasks' grades and general visibility are now shown
- Add refresh button to learning tasks view
- The previous/next buttons and sessions dropdown are now links (you can now
  use [Link Hints] and ctrl-click to open them)
- Class and news feed items can now be opened by simply clicking on their
  background
- Prioritised news feed items now have a red background and important flag
- Make links in Looking Ahead actual links (you can ctrl-click them and they
  now work with [Link Hints])
- The context menu that only says "Copy" is now suppressed
- Workaround a [Link Hints bug](https://github.com/lydell/LinkHints/issues/86)
  that prevents it from seeing links inside lesson plans and such
- Files and folders in Resources are now sorted alphabetically
- Reopening panel tabs and learning tasks when the URL changes
- The main navigation bar is no longer hidden behind masks that don't span the
  entire viewport
- Clicking on "Loading Class Items..." does nothing now instead of reloading
  the current page
- Preload subjects and classes when the page loads
- The option to remember logins is unchecked by default
- The dashboard tab in a user's profile no longer points you to #dsh
- Pages can no longer be scrolled if a window is currently open
- The user's last name is hidden on learning tasks

[Link Hints]: https://lydell.github.io/LinkHints/

## Elements with ID lister

A userscript that adds a "Show elements popup" option to the Monkey Menu which
lists all elements with an ID

## Hide sticky elements

A userscript that adds a button to the monkey menu to hide and unhide sticky
elements

## Image Loader Placeholder Remover

Removes image loading placeholders from images loaded via Javascript, such as
https://closeronline.co.uk, https://wired.com, and https://knowyourmeme.com.

## MediaWiki Redirects Fixer

Fixes redirects of pages with anchors on Wikipedia/MediaWiki instances when
Javascript is disabled

## nightly.link buttons

A script to add [nightly.link](https://nightly.link) buttons on artifacts and
build logs to view them without logging in

## pywb Toolbar Toggler

A userscript that adds a "Close toolbar" and "Open toolbar" button

## Quizizz Force Show Answer

![A man yelling "I DON'T WANNA", followed by sign up/login boxes](https://gitlab.com/blankX/userscripts/-/raw/master/accounts.jpg)

## Tildes with "Newest reply"

A userscript that adds a "newest reply" sort option for topics (see also:
https://gitlab.com/tildes/tildes/-/issues/111)

## Tumblr Unnagger

A script that removes several login/account walls on Tumblr

## Wayback Machine Toolbar Toggler

A userscript that replaces the "close this toolbar" button with one that lets
you reopen the toolbar; its collapsed state is also saved
