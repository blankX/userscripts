// ==UserScript==
// @name        MediaWiki Redirects Fixer
// @namespace   blankie-scripts
// @match       https://*.wikipedia.org/*
// @match       https://wiki.archlinux.org/*
// @match       https://wiki.winehq.org/*
// @match       https://wiki.archiveteam.org/*
// @grant       none
// @version     1.0.1
// @author      blankie
// @description Fixes redirects of pages with anchors on Wikipedia/MediaWiki instances when Javascript is disabled
// @inject-into content
// @run-at      document-end
// ==/UserScript==

"use strict";

function scrollToHash(scrollTo) {
    if (!scrollTo) {
        return;
    }

    let element = document.querySelector(scrollTo);
    if (element) {
        element.scrollIntoView();
    }
}

for (let script of document.querySelectorAll("script")) {
    let match = /;RLCONF=({"[\s\S]+?});RLSTATE={"/.exec(script.textContent);
    if (!match) {
        continue;
    }

    // Handle things like "wgCSPNonce":!1
    let rlconf = match[1].replaceAll(/(?<=[{,])("\w+":)!([01])(?=[,}])/g, function(_, p1, p2) {
        let value = p2 == "0" ? "true" : "false";
        return `${p1}${value}`;
    });
    rlconf = JSON.parse(rlconf);

    if (rlconf.wgInternalRedirectTargetUrl) {
        let url = new URL(rlconf.wgInternalRedirectTargetUrl, location);
        let scrollTo = url.hash;
        if (location.hash) {
            url.hash = location.hash;
            scrollTo = null;
        }

        history.replaceState(null, "", url);
        scrollToHash(scrollTo);
    }

    break;
}