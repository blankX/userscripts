// ==UserScript==
// @name        Archive Site Links
// @namespace   blankie-scripts
// @match       http*://*/*
// @grant       GM_openInTab
// @grant       GM_registerMenuCommand
// @version     1.1.2
// @author      blankie
// @description Adds buttons to the monkey menu to open/archive in the Wayback Machine and archive.today
// @inject-into content
// @noframes
// ==/UserScript==

"use strict";

const ARCHIVE_SITES = [
    {
        name: "Wayback Machine",
        getArchiveUrl: function(url) {
            let newUrl = new URL("https://web.archive.org/save");
            let search = new URLSearchParams([["asl_url", url]]);
            newUrl.search = search.toString();
            return newUrl.href;
        },
        getArchivedUrl: function(url) {
            return `https://web.archive.org/web/${url}${window.location.hash}`;
        },
        getArchivedUrlBase: function() {
            if (window.location.host !== "web.archive.org") return null;
            let match = /^\/web\/(?:[0-9a-z_*]+\/)?(.+)$/.exec(window.location.pathname);
            return match ? match[1] : null;
        },
    },
    {
        name: "archive.today",
        getArchiveUrl: function(url) {
            let newUrl = new URL("https://archive.today");
            newUrl.searchParams.append("url", url);
            return newUrl.href;
        },
        getArchivedUrl: function(url) {
            return `https://archive.today/newest/${url}${window.location.hash}`;
        },
        getArchivedUrlBase: function() {
            // https://en.wikipedia.org/wiki/Archive.today?useskin=vector&
            if (!/^archive\.(today|ph|is|li|vn|fo|md)$/.test(window.location.host)) return null;
            let q = document.querySelector("center form input[name=q]");
            return q ? q.value : null;
        }
    }
];

let url = function() {
    let urlObject = new URL(window.location.href);
    urlObject.hash = "";
    return urlObject.href;
}();
let onArchiveSite;

for (const ARCHIVE_SITE of ARCHIVE_SITES) {
    let urlBase = ARCHIVE_SITE.getArchivedUrlBase(url);
    if (!urlBase) {
        continue;
    }

    if (!/^https?:\/\/|\/\//.test(urlBase)) {
        urlBase = `http://${urlBase}`
    };

    let urlObject = new URL(urlBase);
    urlObject.search = window.location.search;
    url = urlObject.href;
    onArchiveSite = ARCHIVE_SITE;
    break;
}

let archiveSites = 0;
for (const ARCHIVE_SITE of ARCHIVE_SITES) {
    if (archiveSites !== 0) {
        GM_registerMenuCommand("=".repeat(20) + "\u200B".repeat(archiveSites), function() {});
    }
    archiveSites++;

    if (ARCHIVE_SITE !== onArchiveSite) {
        GM_registerMenuCommand(`Open in ${ARCHIVE_SITE.name}`, function() {
            window.location.href = ARCHIVE_SITE.getArchivedUrl(url);
        });
        GM_registerMenuCommand(`Open in ${ARCHIVE_SITE.name} (new tab)`, function() {
            GM_openInTab(ARCHIVE_SITE.getArchivedUrl(url));
        });
    }

    GM_registerMenuCommand(`Archive in ${ARCHIVE_SITE.name}`, function() {
        window.location.href = ARCHIVE_SITE.getArchiveUrl(url);
    });
    GM_registerMenuCommand(`Archive in ${ARCHIVE_SITE.name} (new tab)`, function() {
        GM_openInTab(ARCHIVE_SITE.getArchiveUrl(url));
    });
}

// workaround for the wayback machine not exposing a way to prefill the url input box
let searchParams = new URLSearchParams(window.location.search);
if (window.location.host === "web.archive.org" && window.location.pathname === "/save" && searchParams.has("asl_url")) {
    document.querySelector("#web-save-url-input").value = searchParams.get("asl_url");

    let urlObject = new URL(window.location);
    urlObject.searchParams.delete("asl_url");
    history.replaceState(null, "", urlObject.href);
}