// ==UserScript==
// @name        pywb Toolbar Toggler
// @namespace   blankie-scripts
// @grant       GM.getValue
// @grant       GM.setValue
// @grant       GM_addValueChangeListener
// @version     1.0.1
// @author      blankie
// @description Adds the option to toggle the pywb toolbar
// @inject-into content
// @run-at      document-end
// @noframes
// ==/UserScript==

"use strict";

let app = document.querySelector("body > .app");

function hideToolbar(save) {
    app.style.display = "none";
    document.body.style.setProperty("padding-top", "0", "important");

    let i = document.createElement("i");
    i.className = "fas fa-caret-down";
    let span = document.createElement("span");
    span.style.fontSize = "80%";
    span.append(" Open toolbar");

    let a = document.createElement("a");
    a.role = "link";
    a.setAttribute("style", "background-color: white; padding: 0px 5px 0px 3px; border-radius: 0px 0px 3px 3px; text-decoration: none; cursor: pointer");
    a.append(i, span);

    let div = document.createElement("div");
    div.id = "open-toolbar";
    div.setAttribute("style", "padding-right: 13px; text-align: right; white-space: nowrap; position: fixed; right: 0; top: 0; font-size: 12px");
    div.append(a);

    a.addEventListener("click", function() {
        openToolbar(true);
    }, {passive: true});
    document.body.prepend(div);

    if (save) {
        GM.setValue(window.origin, true).catch(function(error) {
            console.error(`Failed to set ${window.origin} to true: ${error}`);
        });
    }
}
function openToolbar(save) {
    app.style.display = "";
    document.body.style.paddingTop = "";
    document.querySelector("#open-toolbar").remove();

    if (save) {
        GM.setValue(window.origin, false).catch(function(error) {
            console.error(`Failed to set ${window.origin} to false: ${error}`);
        });
    }
}

if (app) {
    let i = document.createElement("i");
    i.className = "fas fa-times";

    let button = document.createElement("button");
    button.className = "btn btn-sm btn-outline-dark";
    button.title = "Hide toolbar";
    button.append(i);

    let li = document.createElement("li");
    li.className = "nav-item";
    li.append(button);

    button.addEventListener("click", function() {
        hideToolbar(true);
    }, {passive: true});
    app.querySelector("#toggles").append(li);

    GM.getValue(window.origin, false).then(function(autoHideToolbar) {
        if (autoHideToolbar) {
            hideToolbar(false);
        }
    }).catch(function(error) {
        console.error(`Failed to fetch ${window.origin}: ${error}`);
    });
    GM_addValueChangeListener(window.origin, function(name, oldValue, autoHideToolbar, remote) {
        if (!remote) {
            return;
        }

        if (autoHideToolbar) {
            hideToolbar(false);
        } else {
            showToolbar(false);
        }
    });
}