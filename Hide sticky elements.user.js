// ==UserScript==
// @name        Hide sticky elements
// @namespace   blankie-scripts
// @match       http*://*/*
// @grant       GM_registerMenuCommand
// @grant       GM_unregisterMenuCommand
// @grant       GM_addStyle
// @version     1.1.1
// @author      blankie
// @description Adds a button to the monkey menu to hide sticky elements
// @inject-into content
// ==/UserScript==

"use strict";

let stickyElements = new Set();
let style = null;
let observer = new MutationObserver(function(mutations) {
    for (let mutation of mutations) {
        handleOneNode(mutation.target);
        for (let node of mutation.addedNodes) {
            handleNode(node);
        }
        for (let node of mutation.removedNodes) {
            handleNode(node, true);
        }
    }
});

let hideStickyElements = true;
let menuId;

function handleOneNode(node, removed = false) {
    if (node.nodeType !== Node.ELEMENT_NODE) {
        return;
    }

    let computedStyle = getComputedStyle(node);
    let isSticky = /^(sticky|fixed|absolute)$/.test(computedStyle.position);
    if (!stickyElements.has(node) && isSticky && !removed) {
        node.classList.add("hseSticky");
        stickyElements.add(node);
    } else if (stickyElements.has(node) && (!isSticky || removed)) {
        node.classList.remove("hseSticky");
        stickyElements.delete(node);
    }
}

function handleNode(node, removed = false) {
    if (node.nodeType !== Node.ELEMENT_NODE) {
        return;
    }

    handleOneNode(node, removed);
    for (let element of node.querySelectorAll("*")) {
        handleOneNode(element, removed);
    }
}

function hideAllStickyElements() {
    style = GM_addStyle(".hseSticky { display: none !important; }");
    handleNode(document.body);
    observer.observe(document.body, {subtree: true, childList: true, attributes: true});
}

function unhideAllStickyElements() {
    observer.disconnect();
    style.remove();
    style = null;

    for (let element of stickyElements) {
        element.classList.remove("hseSticky");
    }
    stickyElements.clear();
}



function registerMenu() {
    if (menuId) {
        GM_unregisterMenuCommand(menuId);
    }

    let caption = hideStickyElements ? "Hide sticky elements" : "Unhide sticky elements";
    menuId = GM_registerMenuCommand(caption, function() {
        (hideStickyElements ? hideAllStickyElements : unhideAllStickyElements)();
        hideStickyElements = !hideStickyElements;
        registerMenu();
    });
}
registerMenu();