// ==UserScript==
// @name        Catbox Max Files Disabler
// @namespace   blankie-scripts
// @match       https://catbox.moe/
// @match       https://litterbox.catbox.moe/
// @grant       none
// @version     1.0
// @author      blankie
// @description Disables Catbox's max files limit
// @inject-into document-end
// @run-as      page
// ==/UserScript==

setTimeout(function() {
    for (var i=0; i < unsafeWindow.Dropzone.instances.length; i++ ) {
        unsafeWindow.Dropzone.instances[i].options.maxFiles = null;
    }
}, 5000);