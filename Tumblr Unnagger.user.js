// ==UserScript==
// @name        Tumblr Unnagger
// @namespace   blankie-scripts
// @match       https://*.tumblr.com/*
// @grant       GM_addStyle
// @version     1.0.2
// @author      blankie
// @description Removes login nags
// @inject-into content
// @run-at      document-end
// ==/UserScript==

"use strict";

let css = `
/* Remove the "Sign up" and "Follow <blog>" buttons */
.sign-up-button, .follow-button {
    display: none !important;
}

/* Remove the top iframe bar on the default theme (e.g. https://azulcrescent.tumblr.com/) */
.tmblr-iframe {
    display: none !important;
}

/* Fix margin from the top iframe bar being hidden */
.nav-wrapper.nav-fixed {
    top: 0;
}

/* Hide fullscreen nag when scrolling */
#glass-container {
    display: none;
}

/* Restore scrolling when fullscreen nag is shown */
html {
    overflow: scroll !important;
}
`;

if (/^\/archive(\/.*)?$/.test(window.location.pathname)) {
    css += `
        /* Remove the "It's time to try Tumblr" nag on /archive */
        #base-container > div:has(style) > div:nth-child(4) {
            display: none;
        }
    `;
}

if (window.location.hostname !== "www.tumblr.com" || window.location.pathname === "/dashboard/iframe") {
    GM_addStyle(css);

    function removeLoginWall(element) {
        element.removeAttribute("data-login-wall-type");
        for (let child of element.querySelectorAll("[data-login-wall-type]")) {
            child.removeAttribute("data-login-wall-type");
        }
    }

    // Remove login walls for elements before Javascript
    removeLoginWall(document.body);

    // Remove login walls for elements added by Javascript
    let observer = new MutationObserver(function(mutations) {
        for (let mutation of mutations) {
            if (mutation.type !== "childList") {
                continue;
            }
            for (let node of mutation.addedNodes) {
                if (node.nodeType !== 1) {
                    continue;
                }
                removeLoginWall(node);
            }
        }
    });
    observer.observe(document.body, {childList: true, subtree: true});
}