// ==UserScript==
// @name        Quizizz Force Show Answer
// @namespace   blankie-scripts
// @match       https://quizizz.com/*
// @grant       none
// @version     1.0
// @author      blankie
// @description Allow you to show answers on Quizizz even if you're not logged in
// @inject-into page
// @run-at      document-end
// ==/UserScript==

unsafeWindow.user = new Proxy(unsafeWindow.user, {
    get: function(target, prop, receiver) {
        if (prop === 'id') {
            var e = new Error();
            var stack_items = e.stack.split('\n');
            for (var i = 0; i < stack_items.length; i++) {
                if (stack_items[i].startsWith('click -> .show-answers-btn@')) {
                    return true;
                }
            }
        }
        return Reflect.get(...arguments);
    }
});