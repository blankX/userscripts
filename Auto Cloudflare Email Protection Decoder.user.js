// ==UserScript==
// @name        Auto Cloudflare Email Protection Decoder
// @namespace   blankie-scripts
// @match       *://*/*
// @grant       none
// @version     1.2.1
// @author      blankie
// @description Automatically decodes Cloudflare Email Protected URLs
// @inject-into content
// @run-at      document-end
// ==/UserScript==

function decode_cf_email(cf_email) {
    var decoded = '', right_xor_arg = parseInt(cf_email.substring(0, 2), 16);
    for (var i = 2; i < cf_email.length; i += 2) {
        decoded += String.fromCharCode(parseInt(cf_email.substring(i, i + 2), 16) ^ right_xor_arg);
    }
    return decodeURIComponent(escape(decoded));
}

function parse_document(document) {
    var tags = document.querySelectorAll('a');
    for (var i = 0; i < tags.length; i++) {
        var tag = tags[i], index_of = tag.href.indexOf('/cdn-cgi/l/email-protection#');
        if (index_of > -1) {
            tag.href = 'mailto:' + decode_cf_email(tag.href.substring(index_of + 28))
        }
    }

    var tags = document.querySelectorAll('.__cf_email__');
    for (var i = 0; i < tags.length; i++) {
        var tag = tags[i], text_node = document.createTextNode(decode_cf_email(tag.getAttribute('data-cfemail')));
        tag.parentNode.replaceChild(text_node, tag);
    }

    var tags = document.querySelectorAll('template');
    for (var i = 0; i < tags.length; i++) {
        parse_document(tags[i].content);
    }
}

try {
    parse_document(unsafeWindow.document);
} catch (e) {
    unsafeWindow.console.error(e);
    throw e;
}